package sari.ika.tugas06

import android.database.sqlite.SQLiteDatabase
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    lateinit var  db : SQLiteDatabase
    lateinit var fragProdi : FragmentProdi
    lateinit var fragMhs : FragmentMahasiswa
    lateinit var ft : FragmentTransaction

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item?.itemId){

        }
        return super.onOptionsItemSelected(item)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bottomNavigationView.setOnNavigationItemSelectedListener(this)
        fragProdi = FragmentProdi()
        fragMhs = FragmentMahasiswa()
        db = DBOpenHelper(this).writableDatabase
    }

    fun getDbObject() : SQLiteDatabase{
        return db
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.itemProdi ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.framelayout,fragProdi).commit()
                framelayout.setBackgroundColor(Color.argb(245,255,255,225))
                framelayout.visibility = View.VISIBLE
            }
            R.id.itemMhs ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.framelayout,fragMhs).commit()
                framelayout.setBackgroundColor(Color.argb(245,255,225,255))
                framelayout.visibility = View.VISIBLE
            }
            R.id.itemAbout -> framelayout.visibility = View.GONE
        }
        return true
    }
}